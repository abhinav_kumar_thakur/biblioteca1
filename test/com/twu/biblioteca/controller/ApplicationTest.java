package com.twu.biblioteca.controller;

import com.twu.biblioteca.view.Console;
import com.twu.biblioteca.model.Library;
import com.twu.biblioteca.model.UnsuccessfulTransferException;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ApplicationTest {
    private Application application;
    private Library library;
    private Console console;

    @Before
    public void setUp() throws Exception {
        library = mock(Library.class);
        console = mock(Console.class);
        application = new Application(library, console);
    }

    @Test
    public void loginShouldAskForUserIdAndPasswordVerification() {
        when(console.read()).thenReturn("username", "password");
        application.login();
        verify(library).addUser("username", "password");
    }

    @Test
    public void logoutShouldRemoveTheUser() {
        application.logout();
        verify(library).removeUser();
    }

    @Test
    public void menuShouldReturnTheListOFAvailableOptionsByTheLibrary() {
        assertEquals("0. Quit\n" +
                "1. List Books\n" +
                "2. Logout\n" +
                "3. Checkout Book\n" +
                "4. Return Book\n" +
                "5. Movie List\n" +
                "6. Checkout Movie\n" +
                "7. User Details\n" +
                "8. User Borrowed List\n" +
                "9. Get Borrower\n" +
                "10. Return Movie", application.menu());
    }

    @Test
    public void selectBookListShouldCallBookList() throws IOException, UnsuccessfulTransferException {
        application.select(1);
        verify(library).bookList();
    }

    @Test
    public void selectCheckoutBookShouldCallCheckoutBook() throws IOException, UnsuccessfulTransferException {
        when(console.read()).thenReturn("book 1");
        application.select(3);
        verify(library).checkoutBook("book 1");
    }

    @Test
    public void selectCheckoutMovieShouldCallCheckoutMovie() throws IOException, UnsuccessfulTransferException {
        when(console.read()).thenReturn("movie");
        application.select(6);
        verify(library).checkoutMovie("movie");
    }

    @Test
    public void selectReturnShouldCallReturn() throws IOException, UnsuccessfulTransferException {
        when(console.read()).thenReturn("book 1");
        application.select(4);
        verify(library).returnBook("book 1");
    }

    @Test
    public void selectShouldCallMovieListForMovieListOption() throws IOException, UnsuccessfulTransferException {
        application.select(5);
        verify(library).movieList();
    }

    @Test
    public void selectShouldDisplayInvalidMessageForInvalidInput() throws IOException, UnsuccessfulTransferException {
        application.select(-1);
        verify(console).write(Application.INVALID_MENU_OPTION_MESSAGE);
    }
}
package com.twu.biblioteca.parser;

import com.twu.biblioteca.parser.UserParser;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserParserTest {
    @Test
    public void userIDShouldReturnUserId() {
        assertEquals("321-4321", UserParser.userID("321-4321,password,user two,two@mail.com,13243554545"));
    }

    @Test
    public void userPasswordShouldReturnUserPassword() {
        assertEquals("password", UserParser.userPassword("321-4321,password,user two,two@mail.com,13243554545"));
    }

    @Test
    public void userNameShouldReturnUserName() {
        assertEquals("user two", UserParser.userName("321-4321,password,user two,two@mail.com,13243554545"));
    }

    @Test
    public void userMailShouldReturnUserMail() {
        assertEquals("two@mail.com", UserParser.userMail("321-4321,password,user two,two@mail.com,13243554545"));
    }

    @Test
    public void userPhoneShouldReturnUserPhoneNumber() {
        assertEquals("13243554545", UserParser.userPhone("321-4321,password,user two,two@mail.com,13243554545"));
    }
}

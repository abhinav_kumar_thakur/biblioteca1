package com.twu.biblioteca.parser;

import com.twu.biblioteca.parser.BookParser;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BookParserTest {
    @Test
    public void nameShouldReturnNameOfBook() {
        assertEquals("book name", BookParser.name("book name,tbook author,2016"));
    }

    @Test
    public void authorShouldReturnAuthorOfBook() {
        assertEquals("book author", BookParser.author("book name,book author,2016"));
    }

    @Test
    public void yearShouldReturnYearOfBook() {
        assertEquals(2016, BookParser.bookYear("book name,book author,2016"));
    }
}

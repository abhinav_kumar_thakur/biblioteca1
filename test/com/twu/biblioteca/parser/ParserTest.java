package com.twu.biblioteca.parser;

import com.twu.biblioteca.parser.Parser;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class ParserTest {

    @Test
    public void optionShouldConvertTheOptionEnteredByTheUserAsInteger() {
        assertEquals(1, Parser.option("1"));
    }

    @Test
    public void optionShouldReturn0ForInvalidInputOption() {
        assertEquals(-1, Parser.option("bla"));
    }

    @Test
    public void getListShouldReturnListOfBooks() throws IOException {
        assertEquals("[book 1,author 1,2011, book 2,author 2,2012, book 3,author 3,2013]", Parser.getList("data/booksTest.txt").toString());
    }
}

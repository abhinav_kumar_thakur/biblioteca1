package com.twu.biblioteca.parser;

import com.twu.biblioteca.model.*;
import com.twu.biblioteca.parser.ListBuilder;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ListBuilderTest {
    private ListBuilder listBuilder;
    private Storage storage;
    private UserList users;
    private List<String> bookList;
    private List<String> movieList;
    private List<String> userList;

    @Before
    public void setUp() throws Exception {
        storage = mock(Storage.class);
        String movie = "Bla,bla,bla,bla";
        String book = "Bla,bla,9999";
        String user = "asdsad,asdasd,asd,asdsad,asdsad";
        bookList = Arrays.asList(book);
        movieList = Arrays.asList(movie);
        userList = Arrays.asList(user);
        users = mock(UserList.class);
        listBuilder = new ListBuilder();
    }

    @Test
    public void movieStorageShouldCallAddToMovie() {
        listBuilder.movieStorage(storage, movieList);
        verify(storage).add(any(Movie.class));
    }

    @Test
    public void bookStorageShouldCallAddToBook() {
        listBuilder.bookStorage(storage, bookList);
        verify(storage).add(any(Book.class));
    }

    @Test
    public void setUserListShouldCallAddUser() {
        listBuilder.setUserList(users, userList);
        verify(users).add(any(User.class));
    }
}

package com.twu.biblioteca.parser;

import com.twu.biblioteca.parser.MovieParser;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MovieParserTest {
    @Test
    public void nameShouldReturnNameOfMovie(){
        assertEquals("movie one", MovieParser.name("movie one,year,director one,rating"));
    }

    @Test
    public void yearShouldReturnNameOfMovie(){
        assertEquals("year",MovieParser.movieYear("movie one,year,director one,rating"));
    }

    @Test
    public void directorShouldReturnNameOfMovie(){
        assertEquals("director one",MovieParser.director("movie one,year,director one,rating"));
    }

    @Test
    public void ratingShouldReturnNameOfMovie(){
        assertEquals("rating",MovieParser.rating("movie one,year,director one,rating"));
    }

}

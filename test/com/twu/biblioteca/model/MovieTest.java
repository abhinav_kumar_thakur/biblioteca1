package com.twu.biblioteca.model;

import com.twu.biblioteca.model.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MovieTest {
    private Movie movie;

    @Before
    public void setUp() throws Exception {
        String year = "2015";
        String rating = "5";
        movie = new Movie("movie name", year, "director", rating);
    }

    @Test
    public void toStringShouldReturnTheMovieDetails() {
        assertEquals("MOVIE: name = movie name | bookYear = 2015 | director = director | rating = 5", movie.toString());
    }

    @Test
    public void hasNameShouldReturnIfMovieHasSameNameAsParameter() {
        assertTrue(movie.hasName("movie name"));
    }
}

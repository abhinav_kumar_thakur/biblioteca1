package com.twu.biblioteca.model;

import com.twu.biblioteca.model.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class UserTest {
    private User user;
    private Storage storage;

    @Before
    public void setUp() throws Exception {
        storage = mock(Storage.class);
        user = new User("123-1234", "password", "user one", "one@mail.com", "9731373314",storage);
    }

    @Test
    public void toStringShouldReturnTheUserDetails() {
        assertEquals("name=user one| mail=one@mail.com | phone=9731373314", user.toString());
    }

    @Test
    public void isValidUserShouldReturnTrueIfInputIsValid() {
        assertTrue(user.isValidUser("123-1234", "password"));
    }

    @Test
    public void addShouldCallStorageAdd(){
        user.add(mock(Book.class));
        verify(storage).add(any(Book.class));
    }

    @Test
    public void removeShoouldClassStorageRemove(){
        user.remove("");
        verify(storage).remove(any(String.class));
    }
}
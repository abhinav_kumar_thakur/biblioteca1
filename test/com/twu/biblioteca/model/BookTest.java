package com.twu.biblioteca.model;

import com.twu.biblioteca.model.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BookTest {
    private Book book;

    @Before
    public void setUp() throws Exception {
        book = new Book("BookName", "Author", 2016);
    }

    @Test
    public void toStringShouldReturnTheDeltailsOfTheBook() {
        String bookDetails = "BOOK: BookName | Author | 2016";
        assertEquals(bookDetails, book.toString());
    }

    @Test
    public void isBookNameShouldReturnTrueIfBookNameIsEqual() {
        book.hasName("BookName");
    }

}
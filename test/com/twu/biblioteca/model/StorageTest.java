package com.twu.biblioteca.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

public class StorageTest {
    private Storage storage;
    private Book book;

    @Before
    public void setUp() throws Exception {
        book = mock(Book.class);
        storage = new Storage();
    }

    @Test
    public void addToBorrowShouldTransferBookFromAvailableListToBorrowedList() throws UnsuccessfulTransferException {
        storage.add(book);
        when(book.hasName("book 1")).thenReturn(true);
        assertEquals(book, storage.addToBorrowed("book 1"));
    }

    @Test(expected = UnsuccessfulTransferException.class)
    public void addToBorrowShouldThrowUnsuccesfulTransferExceptionIfBookIsNotAvailable() throws UnsuccessfulTransferException {
        storage.add(book);
        when(book.hasName("book 1")).thenReturn(false);
        storage.addToBorrowed("book");

    }

    @Test(expected = UnsuccessfulTransferException.class)
    public void addToBorrowShouldThrowUnsuccesfulTransferExceptionIfBookIsAlreadyBorrowed() throws UnsuccessfulTransferException {
        storage.add(book);
        when(book.hasName("book 1")).thenReturn(false);
        storage.addToBorrowed("book 1");
        storage.addToBorrowed("book  1");

    }

    @Test(expected = UnsuccessfulTransferException.class)
    public void addToAvailbaleShouldThrowUnsuccesfulTransferException() throws UnsuccessfulTransferException {
        storage.add(book);
        when(book.hasName("book 1")).thenReturn(true);
        storage.addToBorrowed("book 1");
        storage.addToAvailable("book");
    }

    @Test
    public void addToAvailbaleShouldNotThrowUnsuccesfulTransferExceptionForSuccessfullReturn() throws UnsuccessfulTransferException {
        Exception exception = null;
        try {
            storage.add(book);
            when(book.hasName("book 1")).thenReturn(true);
            storage.addToBorrowed("book 1");
            storage.addToAvailable("book 1");
        }catch (Exception e){
            exception = e;
        }
        assertNull(exception);
    }

    @Test
    public void removeShouldReturnStorableFromStorage(){
        storage.add(book);
        when(book.hasName("book 1")).thenReturn(true);
        assertEquals(book,storage.remove("book 1"));
    }

    @Test
    public void removeShouldReturnNullIfStorableIsNotInStoragge(){
        storage.add(book);
        when(book.hasName("book 1")).thenReturn(false);
        assertNull(storage.remove("book 1"));
    }
}
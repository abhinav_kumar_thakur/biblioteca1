package com.twu.biblioteca.model;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;

import static com.twu.biblioteca.model.Library.WELCOME_MESSAGE;
import static com.twu.biblioteca.model.Storage.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class LibraryTest {
    private Library library;
    private Storage bookStorage;
    private Storage movieStorage;
    private UserList userList;
    private User currentUser;
    private Register register;

    @Before
    public void setUp() throws Exception {
        bookStorage = mock(Storage.class);
        movieStorage = mock(Storage.class);
        userList = mock(UserList.class);
        currentUser = mock(User.class);
        register = mock(Register.class);
        this.library = new Library(bookStorage, movieStorage, userList, register);
        when(userList.verifyUser("userID", "password")).thenReturn(currentUser);
        library.addUser("userID", "password");
    }

    @Test
    public void checkoutMovieShouldAddMovieToUser() throws Exception, UnsuccessfulTransferException {
        when(movieStorage.addToBorrowed("movieNmae")).thenReturn((mock(Movie.class)));
        library.checkoutMovie("movieName");
        verify(currentUser).add(any(Movie.class));
    }

    @Test
    public void checkoutBookShouldAddBookToUser() throws Exception, UnsuccessfulTransferException {
        when(bookStorage.addToBorrowed("bookNmae")).thenReturn((mock(Book.class)));
        library.checkoutMovie("bookName");
        verify(currentUser).add(any(Book.class));
    }

    @Test
    public void welcomeShouldReturnTheWelcomeMessage() {
        assertEquals(WELCOME_MESSAGE, library.welcome());
    }

    @Test
    public void bookListShouldReturnBookList() throws Exception {
        when(bookStorage.toString()).thenReturn("booklist");
        assertEquals("booklist", library.bookList());
    }

    @Test
    public void movieListShouldReturnMovieList() throws Exception {
        when(movieStorage.toString()).thenReturn("movielist");
        assertEquals("movielist", library.movieList());
    }

    @Test
    public void checkoutBookShouldCallAddToBorrowed() throws IOException, UnsuccessfulTransferException {
        when(bookStorage.addToBorrowed("bookName")).thenReturn(new Book("bla", "bla", 1234));
        library.checkoutBook("bookName");
        verify(bookStorage).addToBorrowed("bookName");
    }

    @Test
    public void checkoutBookShouldCallAddEntriesForRegister() throws IOException, UnsuccessfulTransferException {
        when(bookStorage.addToBorrowed("bookName")).thenReturn(new Book("bla", "bla", 1234));
        library.checkoutBook("bookName");
        verify(register).add(any(Book.class), any(User.class));
    }

    @Test
    public void checkoutMovieShouldCallAddToBorrowed() throws IOException, UnsuccessfulTransferException {
        library.checkoutMovie("movieName");
        verify(movieStorage).addToBorrowed("movieName");
    }

    @Test
    public void checkoutMovieShouldCallAddEntriesForRegister() throws IOException, UnsuccessfulTransferException {
        when(movieStorage.addToBorrowed("movieName")).thenReturn(new Movie("bla", "bla", "bla", "bla"));
        library.checkoutMovie("movieName");
        verify(register).add(any(Movie.class), any(User.class));
    }

    @Test
    public void returnBookShouldCallAddToAvailable() throws IOException, UnsuccessfulTransferException {
        when(userList.verifyUser("userID", "password")).thenReturn(currentUser);
        library.addUser("userID", "password");
        library.returnBook("book 1");
        verify(bookStorage).addToAvailable("book 1");
    }

    @Test
    public void returnMovieShouldCallAddToAvailable() throws IOException, UnsuccessfulTransferException {
        when(userList.verifyUser("userID", "password")).thenReturn(currentUser);
        library.addUser("userID", "password");
        library.returnMovie("movie");
        verify(movieStorage).addToAvailable("movie");
    }

    @Test
    public void addUserShouldCallVerifyUser() {
        library.addUser("username", "password");
        verify(userList).verifyUser("username", "password");
    }

    @Test
    public void getBorrowingUserShouldCallForTheUserWhoBorrowed() {
        library.getBorrowingUser("movie");
        verify(register).getBorrower("movie");
    }

    @Test
    public void checkoutMovieShouldReturnUnsuccesfulTransferExceptionMessageForUnsuccesfulCheckout() throws UnsuccessfulTransferException {
        when(movieStorage.addToBorrowed("movie")).thenThrow(UnsuccessfulTransferException.unsuccessfulCheckoutException(UNSUCCESSFUL_CHECKOUT_MESSAGE));
        assertEquals("com.twu.biblioteca.model.UnsuccessfulTransferException: Request is not available.",library.checkoutMovie("movie"));
    }

    @Test
    public void checkoutBookShouldReturnUnsuccesfulTransferExceptionMessageForUnsuccesfulCheckout() throws UnsuccessfulTransferException {
        when(bookStorage.addToBorrowed("book")).thenThrow(UnsuccessfulTransferException.unsuccessfulCheckoutException(UNSUCCESSFUL_CHECKOUT_MESSAGE));
        assertEquals("com.twu.biblioteca.model.UnsuccessfulTransferException: Request is not available.",library.checkoutBook("book"));
    }

    @Test
    public void userBorrowedListShouldReturnCallUserBorList(){
        library.userBorrowedList();
        verify(currentUser).borrowedList();
    }
}

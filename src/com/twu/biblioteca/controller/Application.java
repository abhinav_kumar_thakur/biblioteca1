package com.twu.biblioteca.controller;

import com.twu.biblioteca.view.IOHandler;
import com.twu.biblioteca.model.Library;
import com.twu.biblioteca.model.UnsuccessfulTransferException;

import java.io.IOException;

import static com.twu.biblioteca.parser.Parser.option;

//Understands various functionality of Library
public class Application {
    static final String INVALID_MENU_OPTION_MESSAGE = "Select a valid option!";
    private static final int GET_BORROWER_INDEX = 9;
    private static final int RETURN_MOVIE_INDEX = 10;
    private static final int USER_DETAILS = 7;
    private static final int LOGOUT = 2;
    private static final int USER_BORROWED_LIST = 8;
    private static final Object LOGGED = new Object();
    private static final Object UNLOGGED = new Object();
    private static final int BOOK_LIST_SELECT = 1;
    private static final int QUIT_SELECT = 0;
    private static final int CHECKOUT_BOOK_SELECT = 3;
    private static final int RETURN_BOOK_SELECT = 4;
    private static final int MOVIE_LIST_INDEX = 5;
    private static final String CONTINUE = "continue";
    private static final String QUIT = "quit";
    private static final String SEPERATOR = "********************************************************";
    private static final int CHECKOUT_MOVIE_SELECT = 6;
    private static final String MENU_OPTIONS = "0. Quit\n" +
            "1. List Books\n" +
            "2. Logout\n" +
            "3. Checkout Book\n" +
            "4. Return Book\n" +
            "5. Movie List\n" +
            "6. Checkout Movie\n" +
            "7. User Details\n" +
            "8. User Borrowed List\n" +
            "9. Get Borrower\n" +
            "10. Return Movie";
    private final Library library;
    private IOHandler ioHandler;
    private Object loginStatus = UNLOGGED;

    public Application(Library library, IOHandler ioHandler) {
        this.library = library;
        this.ioHandler = ioHandler;
    }

    String menu() {
        return MENU_OPTIONS;
    }

    String select(int select) throws IOException, UnsuccessfulTransferException {
        switch (select) {
            case BOOK_LIST_SELECT:
                ioHandler.write(library.bookList());
                break;
            case USER_BORROWED_LIST:
                ioHandler.write(library.userBorrowedList());
                break;
            case LOGOUT:
                logout();
                break;
            case USER_DETAILS:
                ioHandler.write(library.userDetails());
                break;
            case CHECKOUT_BOOK_SELECT:
                ioHandler.write("Enter book name");
                ioHandler.write(library.checkoutBook(ioHandler.read()));
                break;
            case CHECKOUT_MOVIE_SELECT:
                ioHandler.write("Enter movie name");
                ioHandler.write(library.checkoutMovie(ioHandler.read()));
                ;
                break;
            case MOVIE_LIST_INDEX:
                ioHandler.write(library.movieList());
                break;
            case RETURN_BOOK_SELECT:
                ioHandler.write("Enter book name");
                ioHandler.write(library.returnBook(ioHandler.read()));
                break;
            case RETURN_MOVIE_INDEX:
                ioHandler.write("Enter movie name");
                ioHandler.write(library.returnMovie(ioHandler.read()));
                break;
            case GET_BORROWER_INDEX:
                ioHandler.write("Enter book name");
                ioHandler.write(library.getBorrowingUser(ioHandler.read()));
                break;
            case QUIT_SELECT:
                return QUIT;
            default:
                ioHandler.write(INVALID_MENU_OPTION_MESSAGE);
                break;
        }
        return CONTINUE;
    }

    public void start() throws IOException, UnsuccessfulTransferException {
        System.out.println(SEPERATOR);
        System.out.println(library.welcome());
        System.out.println(SEPERATOR);
        String input;
        String status = "";
        while (!status.equals(QUIT)) {
            while (loginStatus != LOGGED) {
                ioHandler.write("Please login to continue");
                login();
            }
            ioHandler.write("Select the option :");
            ioHandler.write(menu());
            input = ioHandler.read();
            status = select(option(input));
            System.out.println(SEPERATOR);
        }
        System.out.println("visit us again");
        System.out.println(SEPERATOR);
    }


    void login() {
        ioHandler.write("Enter user name");
        String userName = ioHandler.read();
        ioHandler.write("Enter user password");
        String password = ioHandler.read();
        if (library.addUser(userName, password)) {
            loginStatus = LOGGED;
        }
    }

    void logout() {
        library.removeUser();
        loginStatus = UNLOGGED;
    }
}
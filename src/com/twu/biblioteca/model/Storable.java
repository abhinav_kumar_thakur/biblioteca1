package com.twu.biblioteca.model;

//Understands the objects that can be stored
public interface Storable {
    boolean hasName(String name);

    String toString();
}

package com.twu.biblioteca.model;

import java.io.IOException;

//understands the reference item management system
public class Library {
    static final String WELCOME_MESSAGE = "Bangalore Public Library Welcomes You";
    private final Storage movieStorage;
    private final UserList userList;
    private Storage bookStorage;
    private User currentUser;
    private Register register;

    public Library(Storage bookStorage, Storage movieStorage, UserList userList, Register register) throws IOException {
        this.bookStorage = bookStorage;
        this.movieStorage = movieStorage;
        this.userList = userList;
        this.register = register;
    }

    public String welcome() {
        return WELCOME_MESSAGE;
    }

    public String bookList() throws IOException {
        return bookStorage.toString();
    }

    public String movieList() throws IOException {
        return movieStorage.toString();
    }

    public String checkoutBook(String bookName) {
        try {
            checkout(bookName, bookStorage);
            return Storage.SUCCESSFUL_CHECKOUT_MESSAGE;
        } catch (UnsuccessfulTransferException e) {
            return e.toString();
        }
    }

    public String checkoutMovie(String movieName) {
        try {
            checkout(movieName, movieStorage);
            return Storage.SUCCESSFUL_CHECKOUT_MESSAGE;
        } catch (UnsuccessfulTransferException e) {
            return e.toString();
        }
    }

    private void checkout(String storableName, Storage storage) throws UnsuccessfulTransferException {
        Storable storable = storage.addToBorrowed(storableName);
        currentUser.add(storable);
        register.add(storable, currentUser);
    }

    public String returnBook(String bookName) {
        try {
            returnStorable(bookName, bookStorage);
            return Storage.SUCCESSFUL_RETURN_MESSAGE;
        } catch (UnsuccessfulTransferException e) {
            return e.toString();
        }
    }

    private void returnStorable(String name, Storage storage) throws UnsuccessfulTransferException {
        storage.addToAvailable(name);
        Storable storable = currentUser.remove(name);
        register.clear(storable);
    }

    public String returnMovie(String movieName) {
        try {
            returnStorable(movieName, movieStorage);
            return Storage.SUCCESSFUL_RETURN_MESSAGE;
        } catch (UnsuccessfulTransferException e) {
            return e.toString();
        }
    }

    public boolean addUser(String username, String password) {
        currentUser = userList.verifyUser(username, password);
        return currentUser != null;
    }

    public void removeUser() {
        currentUser = null;
    }

    public String userDetails() {
        return currentUser.toString();
    }

    public String userBorrowedList() {
        return currentUser.borrowedList();
    }

    public String getBorrowingUser(String movie) {
        User user = register.getBorrower(movie);
        if (user != null) {
            return user.toString();
        } else return "Enter Valid Name";
    }
}
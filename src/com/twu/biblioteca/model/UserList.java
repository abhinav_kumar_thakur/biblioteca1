package com.twu.biblioteca.model;

import java.util.ArrayList;
import java.util.List;

//Understands the list of user
public class UserList {
    private List<User> userList = new ArrayList<>();

    public void add(User user) {
        userList.add(user);
    }

    User verifyUser(String username, String password) {
        for (User user : userList) {
            if (user.isValidUser(username, password)) {
                return user;
            }
        }
        return null;
    }
}
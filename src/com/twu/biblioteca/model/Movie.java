package com.twu.biblioteca.model;

//Understands the details for movie
public class Movie implements Storable {
    private final String name;
    private final String year;
    private final String director;
    private final String rating;

    public Movie(String name, String year, String director, String rating) {
        this.name = name;
        this.year = year;
        this.director = director;
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "MOVIE: " + "name = " + name + " | " +
                "bookYear = " + year + " | " +
                "director = " + director + " | " +
                "rating = " + rating;
    }

    public boolean hasName(String name) {
        return this.name.equals(name);
    }
}

package com.twu.biblioteca.model;

//Understands the details of a book
public class Book implements Storable {
    private final String name;
    private final String author;
    private final int year;

    public Book(String name, String author, int year) {
        this.name = name;
        this.author = author;
        this.year = year;
    }

    @Override
    public boolean hasName(String name) {
        return this.name.equals(name);
    }

    @Override
    public String toString() {
        return "BOOK: " + name + " | " + author + " | " + year;
    }

}

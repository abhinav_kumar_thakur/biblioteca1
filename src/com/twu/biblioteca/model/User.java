package com.twu.biblioteca.model;

import java.io.Serializable;

//Keeps track of consummer information
public class User {

    private final String id;
    private final String password;
    private final Serializable name;
    private final String mail;
    private final String phone;
    private Storage storage;

    public User(String id, String password, String name, String mail, String phone, Storage storage) {
        this.id = id;
        this.password = password;
        this.name = name;
        this.mail = mail;
        this.phone = phone;
        this.storage = storage;
    }

    @Override
    public String toString() {
        return "name=" + name +
                "| mail=" + mail + " |" +
                " phone=" + phone;
    }

    boolean isValidUser(String id, String password) {
        return this.id.equals(id) && this.password.equals(password);
    }

    void add(Storable storable) {
        storage.add(storable);
    }

    String borrowedList() {
        return storage.toString();
    }

    Storable remove(String bookName) {
        return storage.remove(bookName);
    }
}

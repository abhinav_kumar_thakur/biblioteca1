package com.twu.biblioteca.model;

import java.util.ArrayList;
import java.util.List;

//understands the collection of storable items
public class Storage {
    static final String UNSUCCESSFUL_CHECKOUT_MESSAGE = "Request is not available.";
    static final String SUCCESSFUL_CHECKOUT_MESSAGE = "Thank you! Enjoy";
    static final String ALREADY_BORROWED_MESSAGE = "Already borrowed";
    static final String SUCCESSFUL_RETURN_MESSAGE = "Thank you for returning";
    static final String UNSUCCESSFULL_RETURN_MESSAGE = "That is not a valid entry";
    private List<Storable> available = new ArrayList<>();
    private List<Storable> borrowed = new ArrayList<>();

    public void add(Storable storable) {
        available.add(storable);
    }

    @Override
    public String toString() {
        String StorableList = "";
        for (Storable storable : available) {
            StorableList += storable.toString() + "\n";
        }
        return StorableList;
    }

    Storable addToBorrowed(String name) throws UnsuccessfulTransferException {
        if (isAlreadyBorrowed(name)) {
            throw UnsuccessfulTransferException.alreadyBorrowedException(ALREADY_BORROWED_MESSAGE);
        }
        for (Storable storable : available) {
            if (storable.hasName(name)) {
                available.remove(storable);
                borrowed.add(storable);
                return storable;
            }
        }
        throw UnsuccessfulTransferException.unsuccessfulCheckoutException(UNSUCCESSFUL_CHECKOUT_MESSAGE);
    }

    private boolean isAlreadyBorrowed(String name) {
        for (Storable storable : borrowed) {
            if (storable.hasName(name)) {
                return true;
            }
        }
        return false;
    }

    void addToAvailable(String name) throws UnsuccessfulTransferException {
        for (Storable storable : borrowed) {
            if (storable.hasName(name)) {
                available.add(storable);
                borrowed.remove(storable);
                return;
            }
        }
        throw UnsuccessfulTransferException.unsuccesfulReturnException(UNSUCCESSFULL_RETURN_MESSAGE);
    }

    Storable remove(String name) {
        Storable storableTemp = null;
        for (Storable storable : available) {
            if (storable.hasName(name)) {
                storableTemp = storable;
            }
        }
        if (storableTemp != null) {
            available.remove(storableTemp);
            return (storableTemp);
        }
        return null;
    }
}
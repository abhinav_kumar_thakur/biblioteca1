package com.twu.biblioteca.model;

import java.util.HashMap;

public class Register {
    private HashMap<Storable, User> entries = new HashMap<>();

    public Register() {
    }

    void add(Storable storable, User user) {
        entries.put(storable, user);
    }

    void clear(Storable storable) {
        entries.remove(storable);
    }

    User getBorrower(String name) {
        for (Storable storable : entries.keySet()) {
            if ((storable.hasName(name))) {
                return entries.get(storable);
            }
        }
        return null;
    }
}
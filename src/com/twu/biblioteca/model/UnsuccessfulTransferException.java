package com.twu.biblioteca.model;

public class UnsuccessfulTransferException extends Throwable {
    private UnsuccessfulTransferException(String message) {
        super(message);
    }

    static UnsuccessfulTransferException alreadyBorrowedException(String alreadyBorrowedMessage) {
        return new UnsuccessfulTransferException(alreadyBorrowedMessage);
    }

    static UnsuccessfulTransferException unsuccessfulCheckoutException(String unsuccessfulCheckoutMessage) {
        return new UnsuccessfulTransferException(unsuccessfulCheckoutMessage);
    }

    static UnsuccessfulTransferException unsuccesfulReturnException(String unsuccessfullReturnMessage) {
        return new UnsuccessfulTransferException(unsuccessfullReturnMessage);
    }
}

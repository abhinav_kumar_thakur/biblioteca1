package com.twu.biblioteca.view;

import java.util.Scanner;

//Understands IO interaction over console
public class Console implements IOHandler {
    private Scanner reader;

    public Console(Scanner reader) {
        this.reader = reader;
    }

    public String read() {
        return reader.nextLine();
    }

    public void write(String string) {
        System.out.println(string);
    }
}

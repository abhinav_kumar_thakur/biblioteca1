package com.twu.biblioteca.view;

//Understands the application interface with external world
public interface IOHandler {
    void write(String message);

    String read();
}
package com.twu.biblioteca.parser;

import com.twu.biblioteca.model.UserList;
import com.twu.biblioteca.model.*;
import java.util.List;

import static com.twu.biblioteca.parser.BookParser.name;

//Understands the generation of storable list
public class ListBuilder {
    public Storage movieStorage(Storage movieStorage, List<String> movieList) {
        for (String movie : movieList) {
            movieStorage.add(new Movie(MovieParser.name(movie), MovieParser.movieYear(movie), MovieParser.director(movie), MovieParser.rating(movie)));
        }
        return movieStorage;
    }

    public Storage bookStorage(Storage bookStorage, List<String> bookList) {
        for (String book : bookList) {
            bookStorage.add(new Book(name(book), BookParser.author(book), BookParser.bookYear(book)));
        }
        return bookStorage;
    }


    public UserList setUserList(UserList users, List<String> userList) {
        for (String user : userList) {
            users.add(new User(UserParser.userID(user), UserParser.userPassword(user), UserParser.userName(user), UserParser.userMail(user), UserParser.userPhone(user),new Storage()));
        }
        return users;
    }
}
package com.twu.biblioteca.parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//Understands the user input
public class Parser {

    public static int option(String option) {
        try {
            return Integer.parseInt(option);
        } catch (Exception exception) {
            return -1;
        }
    }

    public static List<String> getList(String bookListFile) throws IOException {
        FileReader inputReader = new FileReader(bookListFile);
        BufferedReader Reader = new BufferedReader(inputReader);
        String item = "";
        List<String> itemsList = new ArrayList<String>();
        while ((item = Reader.readLine()) != null) {
            itemsList.add(item);
        }
        return itemsList;
    }

}
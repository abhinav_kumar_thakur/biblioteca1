package com.twu.biblioteca.parser;

public class UserParser {
    static final int USER_ID_INDEX = 0;
    static final int USER_PASSWORD_INDEX = 1;
    static final int USER_NAME_INDEX = 2;
    static final int USER_MAIL_INDEX = 3;
    static final int USER_PHONE_INDEX = 4;

    static String userID(String user) {
        return user.split(",")[USER_ID_INDEX];
    }

    static String userPassword(String user) {
        return user.split(",")[USER_PASSWORD_INDEX];
    }

    static String userName(String user) {
        return user.split(",")[USER_NAME_INDEX];
    }

    static String userMail(String user) {
        return user.split(",")[USER_MAIL_INDEX];
    }

    static String userPhone(String user) {
        return user.split(",")[USER_PHONE_INDEX];
    }
}
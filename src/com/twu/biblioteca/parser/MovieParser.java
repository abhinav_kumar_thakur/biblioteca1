package com.twu.biblioteca.parser;

public class MovieParser {
    static final int MOVIE_YEAR_INDEX = 1;
    static final int MOVIE_DIRECTOR_INDEX = 2;
    static final int MOVIE_RATING_INDEX = 3;
    public static final int MOVIE_NAME_INDEX = 0;

    static String movieYear(String movie) {
        return movie.split(",")[MOVIE_YEAR_INDEX];
    }

    static String director(String movie) {
        return movie.split(",")[MOVIE_DIRECTOR_INDEX];
    }

    static String rating(String movie) {
        return movie.split(",")[MOVIE_RATING_INDEX];
    }

    public static String name(String movie) {
        return movie.split(",")[MOVIE_NAME_INDEX];
    }
}
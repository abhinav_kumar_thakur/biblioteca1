package com.twu.biblioteca.parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BookParser {
    private static final int NAME_INDEX = 0;
    private static final int AUTHOR_INDEX = 1;
    private static final int BOOK_YEAR_INDEX = 2;

    public static String name(String book) {
        return book.split(",")[NAME_INDEX];
    }

    static String author(String book) {
        return book.split(",")[AUTHOR_INDEX];
    }

    static int bookYear(String book) {
        return Integer.parseInt(book.split(",")[BOOK_YEAR_INDEX]);
    }
}
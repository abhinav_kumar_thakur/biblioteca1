import com.twu.biblioteca.controller.Application;
import com.twu.biblioteca.model.*;
import com.twu.biblioteca.parser.*;
import com.twu.biblioteca.view.Console;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

//Understands the launching of the library application
public class BiblliotecaApp {

    public static void main(String[] args) throws IOException, UnsuccessfulTransferException {
        Scanner scanner = new Scanner(System.in);
        Console console = new Console(scanner);

        List<String> bookList = Parser.getList("data/booksTest.txt");
        List<String> movieList = Parser.getList("data/moviesTest.txt");
        List<String> users = Parser.getList("data/UsersTest.txt");

        ListBuilder listBuilder = new ListBuilder();
        Storage movieStorage = new Storage();
        Storage bookStorage = new Storage();
        UserList userList = new UserList();
        Register register = new Register();

        movieStorage = listBuilder.movieStorage(movieStorage, movieList);
        bookStorage = listBuilder.bookStorage(bookStorage, bookList);
        userList = listBuilder.setUserList(userList, users);

        Library library = new Library(bookStorage, movieStorage, userList, register);

        Application application = new Application(library, console);
        application.start();
    }
}
